//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tabula.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ContentNote
    {
        public System.Guid Id { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public System.Guid CatalogId { get; set; }
        public System.Guid UserProfileId { get; set; }
        public string ImageUrl { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<System.DateTime> LastUpdate { get; set; }
        public Nullable<long> Like_Count { get; set; }
        public Nullable<long> Comment_Count { get; set; }
        public string Short_Content { get; set; }
        public string Video_Url { get; set; }
        public string Author { get; set; }
    
        public virtual Catalog Catalog { get; set; }
        public virtual UserProfile UserProfile { get; set; }
    }
}
