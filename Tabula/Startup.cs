﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Tabula.Startup))]
namespace Tabula
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
