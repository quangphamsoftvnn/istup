﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Tabula.Models;

namespace Tabula.Controllers
{
    public class ContentNoteController : Controller
    {
        private TabulaDatabaseEntities db = new TabulaDatabaseEntities();

        // GET: /ContentNote/
        public ActionResult Index()
        {
            var contentnotes = db.ContentNotes.Include(c => c.Catalog).Include(c => c.UserProfile);
            return View(contentnotes.ToList());
        }

        // GET: /ContentNote/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContentNote contentnote = db.ContentNotes.Find(id);
            if (contentnote == null)
            {
                return HttpNotFound();
            }
            return View(contentnote);
        }

        // GET: /ContentNote/Create
        public ActionResult Create()
        {
            ViewBag.CatalogId = new SelectList(db.Catalogs, "Id", "Name");
            ViewBag.UserProfileId = new SelectList(db.UserProfiles, "Id", "UserName");
            return View();
        }

        // POST: /ContentNote/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Name,Content,CatalogId,UserProfileId,ImageUrl,CreateDate,LastUpdate,Like_Count,Comment_Count,Short_Content,Video_Url,Author")] ContentNote contentnote)
        {
            if (ModelState.IsValid)
            {
                contentnote.Id = Guid.NewGuid();
                db.ContentNotes.Add(contentnote);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CatalogId = new SelectList(db.Catalogs, "Id", "Name", contentnote.CatalogId);
            ViewBag.UserProfileId = new SelectList(db.UserProfiles, "Id", "UserName", contentnote.UserProfileId);
            return View(contentnote);
        }

        // GET: /ContentNote/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContentNote contentnote = db.ContentNotes.Find(id);
            if (contentnote == null)
            {
                return HttpNotFound();
            }
            ViewBag.CatalogId = new SelectList(db.Catalogs, "Id", "Name", contentnote.CatalogId);
            ViewBag.UserProfileId = new SelectList(db.UserProfiles, "Id", "UserName", contentnote.UserProfileId);
            return View(contentnote);
        }

        // POST: /ContentNote/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Name,Content,CatalogId,UserProfileId,ImageUrl,CreateDate,LastUpdate,Like_Count,Comment_Count,Short_Content,Video_Url,Author")] ContentNote contentnote)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contentnote).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CatalogId = new SelectList(db.Catalogs, "Id", "Name", contentnote.CatalogId);
            ViewBag.UserProfileId = new SelectList(db.UserProfiles, "Id", "UserName", contentnote.UserProfileId);
            return View(contentnote);
        }

        // GET: /ContentNote/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContentNote contentnote = db.ContentNotes.Find(id);
            if (contentnote == null)
            {
                return HttpNotFound();
            }
            return View(contentnote);
        }

        // POST: /ContentNote/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            ContentNote contentnote = db.ContentNotes.Find(id);
            db.ContentNotes.Remove(contentnote);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
