﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Tabula.Models;

namespace Tabula.Controllers
{
    public class ApiUserProfileController : ApiController
    {
        private TabulaDatabaseEntities db = new TabulaDatabaseEntities();

        // GET api/ApiUserProfile
        public IQueryable<UserProfile> GetUserProfiles()
        {
            return db.UserProfiles;
        }

        // GET api/ApiUserProfile/5
        [ResponseType(typeof(UserProfile))]
        public IHttpActionResult GetUserProfile(Guid id)
        {
            UserProfile userprofile = db.UserProfiles.Find(id);
            if (userprofile == null)
            {
                return NotFound();
            }

            return Ok(userprofile);
        }

        // PUT api/ApiUserProfile/5
        public IHttpActionResult PutUserProfile(Guid id, UserProfile userprofile)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userprofile.Id)
            {
                return BadRequest();
            }

            db.Entry(userprofile).State = System.Data.Entity.EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserProfileExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/ApiUserProfile
        [ResponseType(typeof(UserProfile))]
        public IHttpActionResult PostUserProfile(UserProfile userprofile)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.UserProfiles.Add(userprofile);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (UserProfileExists(userprofile.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = userprofile.Id }, userprofile);
        }

        // DELETE api/ApiUserProfile/5
        [ResponseType(typeof(UserProfile))]
        public IHttpActionResult DeleteUserProfile(Guid id)
        {
            UserProfile userprofile = db.UserProfiles.Find(id);
            if (userprofile == null)
            {
                return NotFound();
            }

            db.UserProfiles.Remove(userprofile);
            db.SaveChanges();

            return Ok(userprofile);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserProfileExists(Guid id)
        {
            return db.UserProfiles.Count(e => e.Id == id) > 0;
        }
    }
}