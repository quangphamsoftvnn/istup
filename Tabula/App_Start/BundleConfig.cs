﻿using System.Web;
using System.Web.Optimization;

namespace Tabula
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                          "~/Scripts/bootstrap.js",
                      "~/Scripts/jquery.imagesloaded.js",
                      "~/Scripts/jquery.cslider.js",
                      "~/Scripts/_references.js",
                      "~/Scripts/carousel.js",
                      "~/Scripts/excanvas.js",
                      "~/Scripts/fancybox.js",
                      "~/Scripts/flexslider.js",
                      "~/Scripts/isotope.js",
                      "~/Scripts/layerslider.kreaturamedia.jquery.js",
                      "~/Scripts/modernizr-2.6.2.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/jquery.placeholder.min.js",
                      "~/Scripts/jquery-easing-1.3.js",
                      "~/Scripts/excanvas.js",
                      "~/Scripts/jquery.flot.js",
                      "~/Scripts/jquery.flot.pie.min.js",
                      "~/Scripts/jquery.flot.stack.js",
                      "~/Scripts/jquery.flot.resize.min.js",
                      "~/Scripts/slider.js",
                      "~/Scripts/modernizr.js",
                      "~/Scripts/twitter.js",
                        "~/Scripts/custom.js",
                        "~/Scripts/retina.js"
                      ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                       "~/Content/bootstrap.css",
                // "~/Content/site.css",
                      "~/Content/bootstrap-responsive.css",
                      "~/Content/fancybox.css",
                      "~/Content/font-awesome-ie7.css",
                      "~/Content/font-awesome.css",
                      "~/Content/glyphicons.css",
                      "~/Content/halflings.css",
                      "~/Content/style.css",
                      "~/Content/layerslider.css",
                      "~/Content/social-icons.css",
                      "~/Content/styleIE.css",
                      "~/Content/styleIE9.css"
                      ));
        }
    }
}
